Stone Wall style

This style was originally created for "ww.keris-treasure-box.com". As this is a highly detailed style some modifactions could be need to suit your taste.

1. Open up panels_extra_styles/styles/stone_wall/stone_wall.inc
2. Go to line 134.
3. Here you find the .css values needed to alter what the block looks like, don't get discouraged its fairly simple.
4. First you want to fix the background to your liking on line 186. Here you will find all the background settings. you only want to change the background-size value.
5. Now that we have the background size to our liking go to line 147 and change only the first value in the top-edge background size. (ie. so if your background width is 200px, your top-edge should be background-size:200px 15px;)
5. So if the first value of your background's background-size(line 189) is 200px, the first value of top-edge's background-size(line 151) and bottom-edge's background-size(line 195) is also 200px.
If the first value of your background's background-size(line 189) is 500px, the first value of top-edge's background-size(line 151) and bottom-edge's background-size(line 195) is also 500px.
6. So if the second value of your background's background-size(line 189) is 200px, the second value of left-edge's background-size(line 177) and right-edge's background-size(line 183) is also 200px.
If the second value of your background's background-size(line 189) is 500px, the second value of left-edge's background-size(line 177) and right-edge's background-size(line 183) is also 500px.

Recap
Line 189's first number = line 151's first number and line 195's first number
Line 189's second number = line 177's second number and line 183's second number